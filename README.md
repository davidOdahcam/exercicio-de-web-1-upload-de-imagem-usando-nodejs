## EXERCÍCIO DE SISTEMAS WEB 1

<strong>As ferramentas utilizadas para sua produção foram:</strong>
- [Node.js](https://nodejs.org/en/);
- [MySQL](https://www.mysql.com)

<strong>Feito por:</strong>
- David dos Santos Machado;

<strong>Dependências:</strong></br>
Para instalar as dependências basta executar o comando "npm install"

<strong>Universidade Federal Rural do Rio de Janeiro</strong></br>
Sistemas de Informação
